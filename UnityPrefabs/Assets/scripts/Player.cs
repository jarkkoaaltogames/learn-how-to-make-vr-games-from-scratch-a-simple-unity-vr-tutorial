﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public GameObject bulletPrefab;

	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        // Player shoot if he/she click mousebutton
        if (Input.GetMouseButtonDown(0))
        {
            GameObject bulletObject = Instantiate(bulletPrefab); // access to the bulletprefab, it appearch only when player press mousebutton
            Bullet bullet = bulletObject.GetComponent<Bullet>();

           Vector3 shootingDirection = new Vector3(
                Random.Range(-0.15f, 0.15f),
                Random.Range(-0.1f,0.2f),
                1
                );

            bullet.shootingDirection = shootingDirection.normalized;
        }
	}
}
