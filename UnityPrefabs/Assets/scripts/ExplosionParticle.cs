﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionParticle : MonoBehaviour {

    public float exposionForce = 150f;
    public float lifetime = 2f;

	// Use this for initialization
	void Start () {
        Vector3 randomDrection = new Vector3(
            Random.Range(-1f, 1f),
            Random.Range(-1f, 1f),
            Random.Range(-1f, 1f)
            );
        float randomForce = Random.Range(0f, exposionForce);
        GetComponent<Rigidbody>().AddForce(randomDrection.normalized * randomForce);
	}
	
	// Update is called once per frame
	void Update () {
        lifetime -= Time.deltaTime;
        if(lifetime <= 0)
        {
            Destroy(gameObject);
        }
	}
}
