﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {
	public float sizeModifier = 2.5f;
	public string newName = "Cubey";
	public bool isRotated = false;

	// Used for initialization.
	void Start(){
		// Debug.Log ("Started");	
		transform.name = ImproveName(newName);
	}

	void Update(){
		// Debug.Log ("Updating!"); // Update script every 1 second
		transform.localScale = Vector3.one * sizeModifier;
	}
	string ImproveName (string originalString){
		return "-[" + originalString + "]-";
	}
}
