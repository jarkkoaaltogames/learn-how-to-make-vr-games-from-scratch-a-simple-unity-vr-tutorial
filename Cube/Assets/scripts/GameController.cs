﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject cubePrefab;
    public GameObject blueCubePrefab;
    public int cubeAmount = 10;
    public float randomArea = 10f;

	// Use this for initialization
	void Start () {
        for (int i=0; i<10; i++)
        {
           GameObject cubeObject =  Instantiate(cubePrefab);
            
            cubeObject.transform.position = new Vector3(
                Random.Range(-randomArea, randomArea),
                Random.Range(-randomArea, randomArea),
                0);
        }

        for(int j=0; j<5; j++)
        {
            GameObject blueCubeObject = Instantiate(blueCubePrefab);

                blueCubeObject.transform.position = new Vector3(
                Random.Range(-randomArea, randomArea),
                Random.Range(-randomArea, randomArea),
                0);
        }


        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
