﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

    public float sizeModifier = 2.5f;
    public string newName = "Cube123";
    public bool isRotated = false;

	// Use this for initialization
	void Start () {

        if (isRotated)
        {
            transform.localEulerAngles = Vector3.one * 45;
        }
	}

    // Update is called once per frame
    void Update () {
		
	}
}
